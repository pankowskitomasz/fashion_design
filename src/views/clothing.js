import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import ClothingS1 from "../components/clothing-s1";

class Clothing extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <ClothingS1/>
            </Container>    
        );
    }
}

export default Clothing;