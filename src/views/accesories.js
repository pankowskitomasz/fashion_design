import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import AccesoriesS1 from "../components/accesories-s1";

class Accesories extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <AccesoriesS1/>
            </Container>    
        );
    }
}

export default Accesories;