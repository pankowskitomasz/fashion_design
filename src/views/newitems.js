import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import NewitemsS1 from "../components/newitems-s1";
import NewitemsS2 from "../components/newitems-s2";

class Newitems extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <NewitemsS1/>
                <NewitemsS2/>
            </Container>    
        );
    }
}

export default Newitems;